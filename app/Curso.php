<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = ['nome', 'id_professor'];

    public function professor()
    {
        $q = $this->belongsTo(Professor::class, 'id_professor');

        if ($q) {
            $q = $q->select();
        }
        return $q;
    }

    public $rules = [
        'nome' => 'required|min:3|max:100',
        'id_professor' => 'required|numeric'
    ];
    public $messages = [
        'nome.required' => 'O campo nome é de preenchimento obrigatório',
        'nome.min:3' => 'O campo nome é de preenchimento obrigatório',
        'nome.max:100' => 'O campo nome é de preenchimento obrigatório',
        'id_professor.required' => 'O campo professor é de preenchimento obrigatório',
        'id_professor.numeric' => 'O campo professor deve conter um número'
    ];
}
