<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = ['nome', 'data_nascimento'];

    public $rules = [
        'nome' => 'required|min:3|max:100',
        'data_nascimento' => 'required|max:10'
    ];
    public $messages = [
        'nome.required' => 'O campo nome é de preenchimento obrigatório',
        'nome.min:3' => 'O campo nome é de preenchimento obrigatório',
        'nome.max:100' => 'Você excedeu o número de caracteres do campo nome',
        'data_nascimento.required' => 'O campo data é de preenchimento obrigatório',
        'data_nascimento.max' => 'O campo data não pode ter mais de 10 carateres'
    ];
}
