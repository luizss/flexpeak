<?php

namespace App\Http\Controllers;

use App\Aluno;
use App\Curso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PDF;

class AlunosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $title = 'Lista de alunos';
        $alunos = Aluno::orderBy('nome')->with('curso_professor')->get();
        $nAlunos = Aluno::count();

        return view('aluno.listaaluno',compact('title','alunos','nAlunos'));
    }

    public function novo(){
        $title = 'Novo aluno';
        $cursos = Curso::orderBy('nome')->get();
        return view('aluno.cadaluno',compact('title','cursos'));
    }
    public function salvar(Request $request){
        $valida = new Aluno();
        $this->validate($request,$valida->rules,$valida->messages);
        $aluno = Aluno::where('nome',$request->nome)->where('cep',$request->cep)->get()->count();

        if($aluno>0){
            Session::flash('msg-warning','Já existe um aluno com esses dados');
            return Redirect::to('aluno/novo');
        }else{
            $data = $request->all();
            $create = Aluno::create($data);
            if($create){
                Session::flash('msg-sucess','Cadastro realizado com sucesso');
                return Redirect::to('aluno/novo');
            }
        }

    }

    public function editar($id){
        $title = 'Editar aluno';
        $cursos = Curso::orderBy('nome')->get();
        $aluno = Aluno::find($id);
        return view('aluno.cadaluno',compact('title','aluno','cursos'));
    }

    public function atualizar($id, Request $request){
        $aluno = Aluno::find($id);
        $data = $request->all();

        $update = $aluno->update($data);

        if($update){
            Session::flash('msg-sucess', 'Edição realizada com sucesso!');
            return Redirect::to('aluno/' . $aluno->id . '/editar');
        }
    }
    public function deletar($id){
        $aluno = Aluno::find($id);
        $aluno->delete();
        Session::flash('msg-sucess', 'Registro excluído com sucesso!');
        return Redirect::to('alunos');
    }

    public function show($id){
        $title = "Detalhes";
        $aluno = Aluno::where('id',$id)->with('curso_professor')->get()->first();
        $nAluno = Aluno::where('id',$id)->get()->count();
        if($nAluno==0){
            Session::flash('msg-warning','Aluno não encontrado');
            return Redirect::to('alunos');
        }else{
            return view('aluno.details',compact('title','aluno'));
        }
    }
    public function relatorioPDF($p){
        $title = 'Relatório';
        $alunos = Aluno::orderBy('nome')->with('curso_professor')->get();
        $nAlunos = Aluno::count();
        $pdf = PDF::loadView('aluno/relatorio',compact('alunos','nAlunos','title'));
        if($p=='view'){
            $pdf = $pdf->stream();
        }else if($p=='download'){
            $pdf = $pdf->download('relatorio.pdf');
        }else{
            Session::flash('msg-warning', 'url inválida!');
            $pdf = Redirect::to('alunos');
        }
        return $pdf;
    }
}
