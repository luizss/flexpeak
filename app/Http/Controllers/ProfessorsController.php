<?php

namespace App\Http\Controllers;

use App\Professor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProfessorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $valida  = new Professor();
        $this->validate($request,$valida->rules,$valida->messages);

        $data = $request->all();
        $professor = Professor::create($data);
        if ($professor) {
            Session::flash('msg-sucess', 'Cadastro realizado com sucesso!');
            return Redirect::to('professor/novo');
        }

    }
    public function editar($id){
        $title = 'Editar professor';
        $professor = Professor::find($id);
        return view('professor.cadprofessor',['professor'=>$professor,'title'=>$title]);
    }

    public function atualizar($id, Request $request){
        $professor = Professor::find($id);
        $data = $request->all();
        $professor->update($data);
        if($professor){
            Session::flash('msg-sucess', 'Edição realizada com sucesso!');
            return Redirect::to('professor/'.$professor->id.'/editar');
        }
    }

    public function deletar($id){
       $professor = Professor::find($id);
        $professor->delete();
        Session::flash('msg-sucess', 'Registro excluído com sucesso!');
        return Redirect::to('professores');
    }
    public function lista()
    {
        $title = 'Lista de professores';
        $professores = Professor::orderBy('nome')->get();
        $nProfessores = Professor::count();
        return view('professor.listaprofessor', ['professores' => $professores, 'nProfessores' => $nProfessores,'title'=>$title]);
    }

    public function novo()
    {
        $title = 'Novo professor';
        return view('professor.cadprofessor',['title'=>$title]);
    }
}
