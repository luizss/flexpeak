<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Professor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CursosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $title = 'Lista de cursos';
        $cursos = Curso::orderBy('nome')->with('professor')->get();
        $ncursos = Curso::count();
        return view('curso.listacurso',['cursos'=>$cursos,'ncursos'=>$ncursos,'title'=>$title]);
    }
    public function novo(){
        $title = 'Novo curso';
        $professores = Professor::select('id','nome')->orderBy('nome')->get();
        return view('curso.cadcurso',compact('title','professores'));
    }

    public function salvar(Request $request){
        $valida = new Curso();
        $this->validate($request,$valida->rules,$valida->messages);

        $nCurso = Curso::where('nome',$request->nome)->get()->count();
        if($nCurso>0){
            Session::flash('msg-warning', 'Este curso já está cadastrado!');
            return Redirect::to('curso/novo');
        }else{
            $data = $request->all();
            $curso = Curso::create($data);
            if ($curso) {
                Session::flash('msg-sucess', 'Cadastro realizado com sucesso!');
                return Redirect::to('curso/novo');
            }
        }

    }
    public function editar($id){
        $title = 'Editar curso';
        $professores = Professor::orderBy('nome')->get();
        $curso = Curso::find($id);
        return view('curso.cadcurso',compact('title','curso','professores'));
    }

    public function atualizar($id, Request $request){
        $curso = Curso::find($id);
        $data = $request->all();
        $update = $curso->update($data);

        if($update){
            Session::flash('msg-sucess', 'Edição realizada com sucesso!');
            return Redirect::to('curso/' . $curso->id . '/editar');
        }
    }
    public function deletar($id){
        $curso = Curso::find($id);
        $curso->delete();
        Session::flash('msg-sucess', 'Registro excluído com sucesso!');
        return Redirect::to('cursos');
    }
}
