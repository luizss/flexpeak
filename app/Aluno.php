<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = ['nome','data_nascimento','logradouro','numero','bairro','cidade','estado','cep','id_curso'];

    public function curso(){
        $q = $this->belongsTo(Curso::class,'id_curso');
        if($q){
            $q = $q->select();
        }
        return $q;
    }
    public function curso_professor(){
        $q = $this->belongsTo(Curso::class,'id_curso');
        if($q){
            $q = $q->select()->with('professor');
        }
        return $q;
    }
    public $rules = [
        'nome' => 'required|min:3|max:100',
        'data_nascimento' => 'required|max:10',
        'logradouro' => 'required|min:3|max:100',
        'bairro' => 'required|min:3|max:100',
        'cidade' => 'required|min:3|max:100',
        'estado' => 'required|min:2',
        'cep' => 'required|min:8|max:8',
        'id_curso' => 'required|numeric'
    ];
    public $messages = [
        'nome.required' => 'O campo nome é de preenchimento obrigatório',
        'nome.min' => 'O campo nome deve conter entre 3 e 100 caracteres',
        'nome.max' => 'O campo nome deve conter entre 3 e 100 caracteres',
        'data_nascimento.required' => 'O campo data de nascimento é de preenchimento obrigatório',
        'data_nascimento.max' => 'O campo data de nascimento deve conter no máximo 10 caracteres',
        'logradouro.required' => 'O campo logradouro é de preenchimento obrigatório',
        'logradouro.min' => 'O campo logradouro deve conter entre 3 e 100 caracteres',
        'logradouro.max' => 'O campo logradouro deve conter entre 3 e 100 caracteres',
        'cidade.required' => 'O campo cidade é de preenchimento obrigatório',
        'cidade.min' => 'O campo cidade deve conter entre 3 e 100 caracteres',
        'cidade.max' => 'O campo cidade deve conter entre 3 e 100 caracteres',
        'bairro.required' => 'O campo bairro é de preenchimento obrigatório',
        'bairro.min' => 'O campo bairro deve conter entre 3 e 100 caracteres',
        'bairro.max' => 'O campo bairro deve conter entre 3 e 100 caracteres',
        'estado.required' => 'O campo estado é de preenchimento obrigatório',
        'estado.min' => 'O campo estado deve conter 2 caracteres',
        'cep.required' => 'O campo CEP é de preenchimento obrigatório',
        'cep.min' => 'O campo cep deve conter 8 caracteres',
        'cep.max' => 'O campo cep deve conter 8 caracteres',
        'id_curso.required' => 'Selecione um curso',
        'id_curso.numeric' => 'O campo curso deve conter um número'
    ];
}
