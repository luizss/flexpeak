<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login',['title'=>'Login']);
});
/*
 * Rotas para funções - professor
 */
Route::get('professores','ProfessorsController@lista');
Route::get('professor/novo','ProfessorsController@novo');
Route::get('professor/{professor}/editar','ProfessorsController@editar');
Route::patch('professor/{professor}','ProfessorsController@atualizar');
Route::delete('professor/{professor}','ProfessorsController@deletar');
Route::post('professor/salvar','ProfessorsController@store');

/*
 * Rotas para funções do curso
 */
Route::get('cursos','CursosController@index');
Route::get('curso/novo','CursosController@novo');
Route::post('curso/salvar','CursosController@salvar');
Route::get('curso/{curso}/editar','CursosController@editar');
Route::patch('curso/{curso}','CursosController@atualizar');
Route::delete('curso/{curso}','CursosController@deletar');
/*
 * Rotas para aluno
 */
Route::get('alunos','AlunosController@index');
Route::get('aluno/novo','AlunosController@novo');
Route::post('aluno/salvar','AlunosController@salvar');
Route::get('aluno/{aluno}/editar','AlunosController@editar');
Route::patch('aluno/{aluno}','AlunosController@atualizar');
Route::delete('aluno/{aluno}','AlunosController@deletar');
Route::get('aluno/relatorio/{pdf}','AlunosController@relatorioPDF');
Route::get('aluno/view/{aluno}','AlunosController@show');
/*
 * Rotas de autenticação
 */
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
