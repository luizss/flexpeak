@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastro de professor
                        <a class="pull-right" href="{{url('professores')}}">Lista de professores</a>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                            @endif
                            <!--mensagem de validação-->
                            @if(isset($errors) && count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            @endif

                            <!--mensagem de sucesso-->
                            @if(Session::has('msg-sucess'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {{ Session::get('msg-sucess') }}
                                </div>
                            @endif

                            @if(Request::is('*/editar'))
                                {!! Form::model($professor, ['method'=>'PATCH','url'=>'professor/'.$professor->id]) !!}
                            @else
                                {!! Form::open(['url'=>'professor/salvar']) !!}
                            @endif
                            {!! Form::label('nome','Nome') !!}
                            {!!
                            Form::input('text','nome',null,['class'=>'form-control','auto-focus','placeholder'=>'Nome do professor'])
                            !!}

                            {!! Form::label('data_nascimento','Data de nascimento') !!}
                            {!!
                            Form::date('data_nascimento',null,['class'=>'form-control
                            form-group','auto-focus'])
                            !!}

                            {!! Form::submit('Salvar',['class'=>'btn btn-primary']) !!}

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection