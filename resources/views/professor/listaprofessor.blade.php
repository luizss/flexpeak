@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Lista de professores
                        <a class="pull-right" href="{{url('professor/novo')}}">Novo professor</a>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(Session::has('msg-sucess'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ Session::get('msg-sucess') }}
                            </div>
                        @endif

                        @if($nProfessores==0)
                            <div class="alert alert-warning">Nenhum registro</div>
                        @else
                            <table class="table table-responsive">
                                <th>Nome</th>
                                <th>Data de nascimento</th>
                                <th>Ações</th>
                                <tbody>
                                @foreach($professores as $professor)
                                    <tr>
                                        <td>{{ $professor->nome }}</td>

                                        <td>{{ $data = substr($professor->data_nascimento,8,2) . "/" .substr($professor->data_nascimento,5,2) . "/" . substr($professor->data_nascimento,0,4) }}
                                        </td>

                                        <td>
                                            <a href="{{ url('professor/'.$professor->id.'/editar') }}"
                                               class="btn btn-default btn-sm">Editar
                                            </a>

                                            {!!
                                            Form::open(['method'=>'DELETE','url'=>'professor/'.$professor->id,'style'=>'display:inline'])
                                            !!}
                                            <button class="btn btn-default btn-sm"
                                                    onclick="return deletar({{ $professor->id }})">Excluir
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection