@extends('layouts.app')

@section('content')
    <div class="row" id="row-login">
        <div class="col-md-8">

        </div>

        <div class="col-md-4">
                <div class="panel panel-default" id="panel-login-default">
                        
        
                        <div class="panel-body" id="panel-login-body">
                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
        
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">E-mail</label>
                                
                                
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Seu e-mail">
        
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    
                                </div>
        
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" >Senha</label>
        
                                    <input id="password" type="password" class="form-control" name="password" required placeholder="Sua senha">
        
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>
        
                                <div class="form-group">
                                   
                                        <button type="submit" class="btn btn-primary">
                                            Login
                                        </button>
        
                                </div>
                            </form>
                        </div>
                    </div>
        </div>
    </div>

@endsection
