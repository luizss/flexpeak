@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastro de curso
                        <a class="pull-right" href="{{url('cursos')}}">Lista de cursos</a>
                    </div>

                    <div class="panel-body">
                        <!--mensagens de validação-->
                        @if(isset($errors) && count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <!--mensagem de aviso-->
                        @if(Session::has('msg-warning'))
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ Session::get('msg-warning') }}
                            </div>
                        @endif
                        <!--Mensagem de sucesso-->
                        @if(Session::has('msg-sucess'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ Session::get('msg-sucess') }}
                            </div>
                        @endif
                            <!-- DEFININDO AS AÇÕES DO FORMULÁRIO -->
                        @if(isset($curso))
                            {{ Form::model($curso, ['method'=>'PATCH','url'=>'curso/'.$curso->id]) }}
                        @else
                            {{ Form::open(['url'=>'curso/salvar']) }}
                        @endif
                            <!-- INÍCIO DOS CAMPOS -->
                            <div class="form-group">
                                {{ Form::label('nome','Nome do curso') }}
                                {{ Form::input('text','nome',null,['class'=>'form-control','placeholder'=>'Curso:']) }}
                            </div>

                            <div class="form-group">

                                {{ Form::label('id_professor','Professor:') }}

                                <select id="id_professor" name="id_professor" class="form-control">
                                    <option value="">Escolha o professor</option>
                                    @foreach($professores as $professor)
                                        <option value="{{ $professor->id }}"
                                                @if(isset($curso) && $curso->id_professor==$professor->id)
                                                    selected
                                                @endif
                                        >{{ $professor->nome }}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                {{ Form::submit('Salvar',['class'=>'btn btn-primary']) }}
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection