@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Lista de cursos
                        <a class="pull-right" href="{{url('curso/novo')}}">Novo curso</a>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if(Session::has('msg-sucess'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ Session::get('msg-sucess') }}
                            </div>
                        @endif

                        @if($ncursos==0)
                            <div class="alert alert-warning">Nenhum registro</div>
                        @else
                            <table class="table table-responsive">
                                <th>Nome</th>
                                <th>Professor</th>
                                <th>Ações</th>
                                <tbody>
                                @foreach($cursos as $curso)
                                    <tr>
                                        <td>{{ $curso->nome }}</td>

                                        <td>{{ $curso->professor->nome }}</td>

                                        <td>
                                            <a href="{{ url('curso/'.$curso->id.'/editar') }}"
                                               class="btn btn-success btn-sm">Editar
                                            </a>

                                            {{
                                            Form::open(['method'=>'DELETE','url'=>'curso/'.$curso->id,'style'=>'display:inline'])
                                            }}
                                            <button class="btn btn-danger btn-sm"
                                                    onclick="return deletar({{ $curso->id }})">Excluir
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection