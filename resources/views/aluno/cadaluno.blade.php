@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cadastro de aluno
                        <a class="pull-right" href="{{url('alunos')}}">Lista de alunos</a>
                    </div>

                    <div class="panel-body">
                        <!--mensagens de validação-->
                        @if(isset($errors) && count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                                    <!--mensagem de aviso-->
                        @if(Session::has('msg-warning'))
                            <div class="alert alert-warning">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                               {{ Session::get('msg-warning') }}
                            </div>
                        @endif
                                        <!--Mensagem de sucesso-->
                        @if(Session::has('msg-sucess'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('msg-sucess') }}
                            </div>
                        @endif
                                            <!-- DEFININDO AS AÇÕES DO FORMULÁRIO -->
                        @if(isset($aluno))
                               <form class="form" method="POST" action="{{ url('aluno/'.$aluno->id.'') }}">
                               {{ method_field('PATCH') }}
                        @else
                                <form class="form" method="POST" action="{{ url('aluno/salvar') }}">
                        @endif
                        {{ csrf_field() }}
                        <!-- INÍCIO DOS CAMPOS -->
                        <div class="form-group">
                            <label for="nome">Nomem do aluno</label>
                            <input id="nome" name="nome" class="form-control" placeholder="Nome do aluno" value="{{ $aluno->nome or old('nome') }}">
                        </div>
                                    <div class="form-group">
                                        <label for="data_nascimento">Data de nascimento</label>
                                        <input type="date" id="data_nascimento" name="data_nascimento" class="form-control" value="{{ $aluno->data_nascimento or old('data_nascimento') }}">
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label for="cep">CEP</label>
                                            <input type="text" id="cep" name="cep" class="form-control" placeholder="Informe o CEP" value="{{ $aluno->cep or old('cep') }}">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="uf">Estado</label>
                                            <input type="text" id="uf" name="estado" class="form-control" value="{{ $aluno->estado or old('estado') }}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-9">
                                            <label for="rua">Logradouro</label>
                                            <input type="text" id="rua" name="logradouro" class="form-control" value="{{ $aluno->logradouro or old('logradouro') }}">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="numero">Número</label>
                                            <input type="text" id="numero" name="numero" class="form-control" value="{{ $aluno->numero or old('numero') }}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label for="bairro">Bairro</label>
                                            <input type="text" id="bairro" name="bairro" class="form-control" value="{{ $aluno->bairro or old('bairro') }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" id="cidade" name="cidade" class="form-control" value="{{ $aluno->cidade or old('cidade') }}">
                                        </div>
                                    </div>

                        <div class="form-group">
                            <label for="id_curso">Escolha o curso</label>
                            <select id="id_curso" name="id_curso" class="form-control">
                                <option value="">Escolha o curso...</option>
                                @foreach($cursos as $curso)
                                    <option value="{{ $curso->id }}"

                                    @if(isset($aluno) && $aluno->id_curso==$curso->id)
                                        selected
                                    @endif
                                    >{{ $curso->nome }}</option>

                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Salvar">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection