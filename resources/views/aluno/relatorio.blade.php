<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->


    <title>{{ $title or config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
        body{background-color: #ffffff;}
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Lista de alunos</h2>
                </div>

                <div class="panel-body">

                    @if($nAlunos==0)
                        <div class="alert alert-warning">Nenhum registro</div>
                    @else
                        <table class="table table-responsive">
                            <tr>
                                <th>Nome</th>
                                <th>curso</th>
                                <th>Professor</th>
                            </tr>
                            <tbody>
                            @foreach($alunos as $aluno)
                                <tr>
                                    <td>{{ $aluno->nome }}</td>

                                    <td>{{ $aluno->curso->nome }}</td>

                                    <td>{{ $aluno->curso_professor->professor->nome }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>