@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}
                        <a class="pull-right" href="{{url('aluno/novo')}}">Novo aluno</a>
                    </div>

                    <div class="panel-body">
                        <!--Dados do aluno-->
                        <!--Nome-->
                        <p><strong>Nome: </strong>{{ $aluno->nome }}</p>

                        <!--data de nascimento-->
                        <p><strong>Data de nascimento: </strong>{{ $data = substr($aluno->data_nascimento,8,2) . "/" .substr($aluno->data_nascimento,5,2) . "/" . substr($aluno->data_nascimento,0,4) }}</p>

                        <!--Endereço completo-->
                        <p><strong>Endereço: </strong>{{ $aluno->logradouro }}, nº {{ $aluno->numero }} - {{ $aluno->bairro }} - {{ $aluno->cidade }}, {{ $aluno->estado }} - {{ $aluno->cep }}</p>

                        <!--curso atual-->
                        <p><strong>Curso atual: </strong>{{ $aluno->curso->nome }}</p>

                        <!--Professor atual-->
                        <p><strong>Professor atual: </strong>{{ $aluno->curso_professor->professor->nome }}</p>

                        <!--botao de exluir-->
                        {{ Form::open(['method'=>'DELETE','url'=>'aluno/'.$aluno->id]) }}
                            <button class="btn btn-danger btn-sm" onclick="return deletar({{ $aluno->id }})">Excluir</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection