@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}
                        <a class="pull-right" href="{{url('aluno/novo')}}">Novo aluno</a>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @if(Session::has('msg-warning'))
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {{ Session::get('msg-warning') }}
                                </div>
                            @endif

                        @if(Session::has('msg-sucess'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {{ Session::get('msg-sucess') }}
                            </div>
                        @endif

                        @if($nAlunos==0)
                            <div class="alert alert-warning">Nenhum registro</div>
                        @else
                            <table class="table table-responsive">
                                <th>Nome</th>
                                <th>curso</th>
                                <th>Professor</th>
                                <th>Ações</th>
                                <tbody>
                                @foreach($alunos as $aluno)
                                    <tr>
                                        <td>{{ $aluno->nome }}</td>

                                        <td>{{ $aluno->curso->nome }}</td>

                                        <td>{{ $aluno->curso_professor->professor->nome }}</td>
                                        <td>
                                            <a href="{{ url('aluno/'.$aluno->id.'/editar') }}"
                                               class="btn btn-default btn-sm">Editar
                                            </a>
                                            <a href="{{ url('aluno/view/'.$aluno->id) }}" class="btn btn-success btn-sm">Detalhes</a>

                                            {!!
                                            Form::open(['method'=>'DELETE','url'=>'aluno/'.$aluno->id,'style'=>'display:inline'])
                                            !!}
                                            <button class="btn btn-danger btn-sm"
                                                    onclick="return deletar({{ $aluno->id }})">Excluir
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <a href="{{ url('aluno/relatorio/'.$pdf='view'.'') }}" class="btn btn-success btn-sm">Ver relatório</a>
                            <a href="{{ url('aluno/relatorio/'.$pdf='download'.'') }}" class="btn btn-success btn-sm">Download relatório</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection